---
title: Week 01
bookFlatSection: false
weight: 1
---

# First Week Ever

## Setting up the command line

The text-based interface of a command line can seem like a crude and lonesome place for the uninitiated. Apple has not done very much to make their Terminal app user-friendly. A little setup can make the experience much more cozy. 

### iTerm2
The first step is to have a terminal emulator that can be configured to your liking. [iTerm2](https://iterm2.com/) is free and widely used. Download it and install.

### Homebrew
Homebrew is a package manager for Mac. To install it, simply copy and paste the line below into terminal. It will include Xcode and Git. This might take some time to run.

```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

### Oh My Zsh!

First of all 

```
brew install zsh zsh-completions
```

This gives a little more visual cues e.g. about the status of Git, which can be helpful for keeping on track in your process.

```
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

In the next steps you’ll need to edit the hidden file ~/.zshrc configuration file. To show hidden files in finder, press `cmd + shift + .`  You can also open the file through your command line 

### Add Powerlevel9k Zsh Theme

Add Powerlevel9k Zsh Theme run

```
git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k
```

Then edit this file

```
open .zshrc 
```

By adding this:

```
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(dir rbenv vcs)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status root_indicator background_jobs history time)
POWERLEVEL9K_PROMPT_ON_NEWLINE=true

# Add a space in the first prompt
POWERLEVEL9K_MULTILINE_FIRST_PROMPT_PREFIX="%f"
# Visual customisation of the second prompt line
local user_symbol="$"
if [[ $(print -P "%#") =~ "#" ]]; then
    user_symbol = "#"
fi
POWERLEVEL9K_MULTILINE_LAST_PROMPT_PREFIX="%{%B%F{black}%K{yellow}%} $user_symbol%{%b%f%k%F{yellow}%} %{%f%}"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="powerlevel9k/powerlevel9k"
```

### Powerline fonts

To make the interface even better you could install [Powerline fonts from GitHub](https://github.com/powerline/fonts). I recommend downloading [Meslo](https://github.com/powerline/fonts/blob/master/Meslo%20Slashed/Meslo%20LG%20M%20Regular%20for%20Powerline.ttf). To install fonts, I do not use FontBook. I like to think about fonts as files on my computer, so I just move the downloaded font-file to ~ /Library/Fonts

### Vertical cursor
iTerm2 → Preferences → Profiles → Text
→ Cursor : ✓ Vertical Bar 
→ Blinking cursor : ✓ ON

### Set your aliases

Aliases are a way of bundling up commands into simple words (or abbreviations or anything you can remember easily). We are going to make an alias for opening Sublime Text. First a test: If you run this command, it should open Sublime Text

```
open -a /Applications/Sublime\ Text.app
```

If Sublime Text opens, all is good with your setup. After that, we just need to add one line to your Zsh configuration file. Open the file **.zshrc** for editing using **nano**, a text editor within the command line:

```
nano ~/.zshrc
```

Then add this command to the end of **.zshrc**:

```
alias sublime="open -a /Applications/Sublime\ Text.app"
```

Press `control + X` then choose `Y` to save your changes.

Then run this command to refresh the **.zshrc**

```
source ~/.zshrc
```


When this is done, you should be able to open any file with Sublime Text by doing `sublime name_of_file`, or open the current folder like this:

```
sublime .
```


### ... more tips

There are several articles about how to best set up your iterm: 
- [The one I read first](https://medium.com/@Clovis_app/configuration-of-a-beautiful-efficient-terminal-and-prompt-on-osx-in-7-minutes-827c29391961)
- [A newer one](https://medium.com/@jeantimex/how-to-configure-iterm2-and-vim-like-a-pro-on-macos-e303d25d5b5c)


## Documentation for Fab Academy

### Plan

I need a site with a structure that should be something like this when it is done:

- **Home**
	- Introduction
- **About**
	- Short Bio
- **Final Project**
	- Description of the idea
- **Assignments**
	- **Week 01**
		- Process pictures and text
	- **Week 02**
		- Process pictures and text
	- **Week 03**
		- Process pictures and text
	- **etc.**

### GitLab

I store my sites locally in the **Sites** directory on my Mac. Now we are going to connect that to the GitLab repository. Let's do this in the command line for efficiency. First we navigate to the right directory like this:

```
cd sites
```

Then clone what is in GitLab to my local directory:

```
git clone git@gitlab.com:niklasekholm/fab-academy.git
```

This makes a folder in your **Sites** called **fab-academy** as the repository on GitLab.

Let's open the directory in Sublime Text with the alias that I set up in the iTerm Setup.

```
sublime fab-academy
```


### Hugo

Now let's continue setting up the new site with Hugo.

```
brew install hugo
```

Inside the chosen directory, generate the files needed for a new Hugo project like this:

```
hugo new site .
```

Then commit everything and push it to GitLab:

```
git add .
```

```
git commit -m "start over with Hugo"
```

```
git push
```

To make see that it works, we can open the site:
 
```
open http://gitlab.com/niklasekholm/fab-academy/
```

### Hugo Theme

I have chose to test a theme simply called [Book](https://themes.gohugo.io/hugo-book/). This is how to install it:

```
git clone https://github.com/alex-shpak/hugo-book themes/book
```

There is an example site embedded in the theme, which you can copy to the **content** directory of your site to get started. It will also remain in the theme folder unchanged for future reference.

```
cp -R themes/book/exampleSite/content .
```


### Hugo Server

To make the theme work with the local server provided by Hugo, we need to edit the **config.toml** file, located at the root of your site. However this is not a **.toml** is not a format associated with any app on my computer, so you can either just open Finder and drag it to your favourite text editor — or — you could make an alias to your text editor for convenience in the future.

So once we get the **config.toml** file open in Sublime, add this line to the end:

```
theme = "book"
```

Now let's see how this looks, by running the local Hugo Server.

```
hugo server
```

It should give you this:

```
...
Web Server is available at http://localhost:1313/ (bind address 127.0.0.1)
Press Ctrl+C to stop
```

While it is running, you will be able to edit the files and see the changes in the browser.

Let's leave that running in there, and open a new iTerm2 window for Git and other stuff. Now lets first open the local site:

```
open http://localhost:1313/
```

### Styling

Doing this requires something not entirely obvious. To make the link to the stylesheet work on when hosted on GitLab, we need to make the links relative. We do this by inserting `canonifyURLs = true to the config.toml file:

```
baseURL = "https://niklasekholm.gitlab.io/fab-academy/"
canonifyURLs = true
languageCode = "en-us"
title = "Fab Academy 2020 Niklas Ekholm"
theme = "book"
```

To see if this works, lets change the fonts to the native sysem fonts instead of the theme's font Roboto. Below are the font stacks that both GitHub and GitLab uses, which I think are very well considered.

For body text:

```
font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Noto Sans", Ubuntu, Cantarell, "Helvetica Neue", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
```

For code:

```
font-family: "Menlo", "DejaVu Sans Mono", "Liberation Mono", "Consolas", "Ubuntu Mono", "Courier New", "andale mono", "lucida console", monospace;
```

so the file  **fab-academy > themes > book > assets > _fonts.scss** will be edited like this:

```
@font-face {
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Noto Sans", Ubuntu, Cantarell, "Helvetica Neue", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
  font-style: italic;
  font-weight: 300;
  font-display: swap;
}
@font-face {
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Noto Sans", Ubuntu, Cantarell, "Helvetica Neue", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
  font-style: normal;
  font-weight: 400;
  font-display: swap;
}

@font-face {
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Noto Sans", Ubuntu, Cantarell, "Helvetica Neue", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
  font-style: normal;
  font-weight: 700;
  font-display: swap;
}


@font-face {
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Noto Sans", Ubuntu, Cantarell, "Helvetica Neue", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
  font-style: normal;
  font-weight: 400;
  font-display: swap;
}

body {
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Noto Sans", Ubuntu, Cantarell, "Helvetica Neue", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
}

code {
  font-family: "Menlo", "DejaVu Sans Mono", "Liberation Mono", "Consolas", "Ubuntu Mono", "Courier New", "andale mono", "lucida console", monospace;
}

```

Also in **fab-academy > themes > book > assets > _main.scss**, I will set the normal letter spacing back:

```
letter-spacing: 0;
```


## Content

I simply organised the files and folders into **content** of the Hugo files according to the plan.