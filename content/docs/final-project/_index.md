---
weight: 3
bookFlatSection: false
title: "Final Assignment"
---

# Final Assignment

I would like to build robotic hands that can type on a typewriter. Ideally they would be configurable to work with different models of old typewriters, so that we can appreciate the subtleties of typewriter aesthetics.

Previous examples of similar project:
[48 Solenoids Transform This 1960s Typewriter into a Computer Printer](https://makezine.com/2015/07/01/48-solenoids-transform-1960s-typewriter-computer-printer/)

