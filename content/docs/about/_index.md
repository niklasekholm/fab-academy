---
weight: 1
bookFlatSection: false
title: "About"
---

# Short Bio

My name is Niklas Ekholm. I am currently a Media Lab student at Aalto University. Previously got a BA from Graphic Design also from Aalto, and for the past decade I have been working as Art Director at a design agency.

